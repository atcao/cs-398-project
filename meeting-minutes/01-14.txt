=============================================================================
Date: Friday, Jan 14, 2022

Team: 101

Present: Anthony Tan Cao, Bilal Saad, David Hu, Joseph Hu
Absent:

===============================================================================

Agenda:
- Compile answers for interviews
- Start working on personas

Notes:
- starting to make persona for students, created 
- folders and text formatting seemed widely desirable among interviewees

Decisions:
- decided that we definitely need text formatting (bold, italics, etc.) as a
feature in our project
- folders  might be out of scope, but it's something that we'll keep in mind

TODO:
- finish last interview
- install tools (Java, IntelliJ)
