=============================================================================
Date: Monday, Jan 31, 2022

Team: 101

Present: Anthony Tan Cao, Bilal Saad, David Hu, Joseph Hu
Absent:

===============================================================================

Agenda:
- start setup for our intellij project
- Setup .gitignore file

Notes:
- thought using about bootstrapfx and jfoenix

Decisions:
- will add dependencies later

TODO:
- Research technologies to use, how to save data, how to work with md files and display in javafx
