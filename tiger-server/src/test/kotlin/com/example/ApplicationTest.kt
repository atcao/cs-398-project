package com.example

import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.auth.*
import io.ktor.util.*
import io.ktor.client.*
import io.ktor.sessions.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import kotlin.test.*
import io.ktor.server.testing.*
import java.io.File
import java.nio.file.Files

class ApplicationTest {
    @BeforeTest
    fun setup() {
        folderPrefix = Files.createTempDirectory("testnotesdir").toString()
    }

    @Test
    fun testLogin() {
        withTestApplication(Application::main) {
            handleRequest(HttpMethod.Get, "/login").apply {
                assertEquals(302, response.status()?.value)
            }
        }
    }

    @Test
    fun testUserid() {
        withTestApplication(Application::main) {
            handleRequest(HttpMethod.Get, "/userid").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("", response.content)
            }
        }
    }

    @Test
    fun testNotesTimestampNoHeader() {
        withTestApplication(Application::main) {
            handleRequest(HttpMethod.Get, "/notes-timestamp").apply {
                assertEquals(HttpStatusCode.BadRequest, response.status())
                assertEquals("did not include userId in header", response.content)
            }
        }
    }

    @Test
    fun testNotesTimestamp() {
        withTestApplication(Application::main) {
            val testDir = File("${folderPrefix}/1-1648211002.zip")
            testDir.createNewFile()
            with(handleRequest(HttpMethod.Get, "/notes-timestamp")  {
                addHeader("userId", "1")
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("{timestamp: 1648211002}", response.content)
            }
        }
    }

    @Test
    fun testNotesNoHeader() {
        withTestApplication(Application::main) {
            with(handleRequest(HttpMethod.Get, "/notes")  {
            }) {
                assertEquals(HttpStatusCode.BadRequest, response.status())
                assertEquals("did not include userId in header", response.content)
            }
        }
    }

    @Test
    fun testNotes() {
        withTestApplication(Application::main) {
            val testDir = File("${folderPrefix}/1-1648211002.zip")
            testDir.createNewFile()
            with(handleRequest(HttpMethod.Get, "/notes")  {
                addHeader("userId", "1")
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }
}