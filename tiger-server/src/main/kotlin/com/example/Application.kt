package com.example

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.sessions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.html.*
import kotlinx.serialization.*
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.lang.Exception

var folderPrefix = "notes"

fun parseUserId(filename: String): String{
    val res = Regex("(.*)-").find(filename) ?: throw Exception("No regex result parseUserId")
    return res.groups[1]?.value ?: throw Exception("couldn't match userId parseUserId")
}

fun parseTimestamp(filename: String): String{
    val res = Regex("(.*)-(.*)\\.zip").find(filename) ?: throw Exception("No regex result parseTimestamp")
    return res.groups[2]?.value ?: throw Exception("couldn't match userId parseTimestamp")
}

fun getFileFromUserId(userId: String): File{
    return File(folderPrefix).listFiles({ _: File, filename: String ->
        filename.startsWith(userId)
    })[0]
}

fun Application.main() {

    install(Sessions) {
        cookie<UserSession>("user_session")
    }

    val httpClient = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    install(Authentication) {
        oauth("auth-oauth-google") {
            urlProvider = { "http://localhost:8080/callback" }
            providerLookup = {
                OAuthServerSettings.OAuth2ServerSettings(
                    name = "google",
                    authorizeUrl = "https://accounts.google.com/o/oauth2/auth",
                    accessTokenUrl = "https://accounts.google.com/o/oauth2/token",
                    requestMethod = HttpMethod.Post,
                    clientId = "479252618647-1ek78mb0h9mv8t5sfktec5sk0vji0rfj.apps.googleusercontent.com",
                    clientSecret = "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",
                    defaultScopes = listOf("openid")
                )
            }
            client = httpClient
        }
    }

    routing {
        authenticate("auth-oauth-google") {
            get("/login") {

            }

            get("/callback") {
                val principal: OAuthAccessTokenResponse.OAuth2? = call.principal()
                call.sessions.set(UserSession(principal?.accessToken.toString()))
                call.respondRedirect("/hello")
            }

        }
        get("/hello") {
            val userSession: UserSession? = call.sessions.get<UserSession>()
            if (userSession != null) {
                try {
                    val userInfo: UserInfo = httpClient.get("https://www.googleapis.com/oauth2/v2/userinfo") {
                        headers {
                            append(HttpHeaders.Authorization, "Bearer ${userSession.token}")
                        }
                    }
                    call.respondRedirect("/userid?tigeruserid=${userInfo.id}")
                } catch (e: Exception) {
                    println(e)
                }
            }
        }
        get("/userid") {
            call.respond("")
        }
        get("/") {
            call.respondHtml {
                body {
                    p {
                        a("/login") { +"Login with Google" }
                    }
                }
            }
        }

        get("/notes-timestamp"){
            val userId = call.request.headers["userId"]
            if (userId == null) call.respondText("did not include userId in header", ContentType.Text.Plain, HttpStatusCode.BadRequest)
            else call.respondText("""{timestamp: ${parseTimestamp(getFileFromUserId(userId!!).name)}}""")
        }

        get("/notes") {
            val userId = call.request.headers["userId"]
            if (userId == null) call.respondText("did not include userId in header", ContentType.Text.Plain, HttpStatusCode.BadRequest)
            else call.respondFile(getFileFromUserId(userId!!));
        }

        put("/notes") {
            withContext(Dispatchers.IO){
                call.receive<InputStream>().use { stream ->
                    val filename = call.request.headers["filename"]
                    if (filename == null) call.respondText("did not include filename in header", ContentType.Text.Plain, HttpStatusCode.BadRequest)
                    else {
                        // delete old versions
                        val userId = parseUserId(filename)
                        val folder = File(folderPrefix)
                        folder.listFiles({ _: File, filename1: String ->
                            filename1.startsWith(userId)
                        }).forEach { file -> file.delete() }

                        val out = FileOutputStream(File("${folderPrefix}/${filename}"))
                        stream.transferTo(out)
                        out.close()
                    }
                }
            }
        }
    }
}

data class UserSession(val token: String)

@Serializable
data class UserInfo(
    val id: String,
//    val name: String,
//    @SerialName("given_name") val givenName: String,
//    @SerialName("family_name") val familyName: String,
    val picture: String,
//    val locale: String
)

