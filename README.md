# CS 398: Application Development

## Winter 2022 Final Report

**Tiger**, a note taking app that leverages the markdown language, written in Kotlin and JavaFX

![](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/uploads/11572f22197fd5775f6c346f298711ee/image.png)

Team: Anthony Cao, Bilal Saad, David Hu, Joseph Hu

The team meets 3 times a week, Monday, Wednesday, and Friday. [Meeting Minutes](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Meeting-Minutes)

## Project Details

1. [Introduction](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Introduction)
1. [Requirements](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Requirements)
1. [Analysis & Design](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Analysis-Design)
1. [Implementation](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Implementation)
1. [Testing](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Testing)

## Usage Instructions

The tiger note taking application can be used like most other note taking apps. Specifically some of the keyboard shortcuts supported are:

- Ctrl+n: creates a new note
- Ctrl+s: saves your note 
- Ctrl+q: quits the application
- Ctrl+m: minimizes the application
- Ctrl+p: export for pdf
- Ctrl+x, Ctrl+c, Ctrl+v: cut, copy, paste

The notes are organized in the sidebar which includes 2 buttons to create new notes and new folders and a sort menu for note organization. Moreover, by double clicking on a note or folder in the sidebar, you can rename that item.

The note editor interface includes the note toolbar which is home to various text formatting features such as: Bold, Italics, Bullet Points, Code Blocks, Headers and Hyperlinks. It also includes a toggle button for the note preview panel.

## Running the Server

The application may be used in an offline mode but if you intend to run the server you will require:
 - OAuth Client ID
 - OAuth Client Secret

to be defined as environment variables.

Please contact us for the values if you wish to run the server. 

## Installation Instructions
Below is the installer for the latest version from Sprint 3, 3.0.0. 

**Note**: If installing on Mac OS you must follow the extra steps after installation to add the necessary permissions for the app to run.

* Mac OS
  - [Version 3.0.0](https://git.uwaterloo.ca/atcao/cs-398-project/-/raw/master/installers/tiger-3.0.0.dmg?inline=false)
  - [Extra Installation instructions](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Mac-OS-Installation-Instructions)
* Windows
  - [Version 3.0.0](https://git.uwaterloo.ca/atcao/cs-398-project/-/raw/master/installers/tiger-3.0.0.msi?inline=false)

## Releases
* [Version 1.1](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Sprint-1-Release) - Sprint 1 release
* [Version 1.2](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Sprint-2-Release) - Sprint 2 release
* [Version 1.3](https://git.uwaterloo.ca/atcao/cs-398-project/-/wikis/Sprint-3-Release) - Sprint 3 release

## Third-Party Libraries
- JavaFX
- Ktor
- openhtmltopdf
- flexmark
- zip4j
- jlink
- JUnit

## License 

[MIT License](https://git.uwaterloo.ca/atcao/cs-398-project/-/blob/master/LICENSE)
