module com.caohusaad.family.tiger {
    requires javafx.controls;
    requires javafx.fxml;
    requires kotlin.stdlib;
    requires markdown.jvm;
    requires openhtmltopdf.pdfbox;
    requires org.json;
    requires javafx.web;
    requires flexmark.util.ast;
    requires flexmark;
    requires flexmark.util.data;
    requires java.net.http;
    requires zip4j;
    requires kotlinx.coroutines.core.jvm;

    opens com.caohusaad.family.tiger to javafx.fxml;
    exports com.caohusaad.family.tiger;

    opens com.caohusaad.family.tiger.controllers to javafx.fxml;
    exports com.caohusaad.family.tiger.controllers;
}