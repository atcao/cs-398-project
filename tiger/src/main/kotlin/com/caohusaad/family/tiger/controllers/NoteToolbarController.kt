package com.caohusaad.family.tiger.controllers

import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ComboBox
import javafx.scene.layout.AnchorPane

interface NoteToolbarControllerDelegate {
    fun toggleSelectionSurround(text: String)
    fun toggleSelectionSurround(prefix: String, suffix: String)
    fun togglePrefix(prefix: String)
}

interface ToggleControllerDelegate {
    fun togglePreview()
}

class NoteToolbarController {

    var delegate: NoteToolbarControllerDelegate? = null
    var toggleControllerDelegate: ToggleControllerDelegate? = null

    @FXML
    private lateinit var noteToolbar: AnchorPane

    @FXML
    private lateinit var boldButton: Button

    @FXML
    private lateinit var italicsButton: Button

    @FXML
    private lateinit var bulletButton: Button

    @FXML
    private lateinit var codeButton: Button

    @FXML
    private lateinit var hyperlinkButton: Button

    @FXML
    private lateinit var headingComboBox: ComboBox<String>

    @FXML
    private lateinit var livePreviewButton: Button

    // Event Handlers
    @FXML
    private fun onBoldButtonClick() {
        delegate?.toggleSelectionSurround("**")
    }

    @FXML
    private fun onItalicsButtonClick() {
        delegate?.toggleSelectionSurround("_")
    }

    @FXML
    private fun onBulletButtonClick() {
        delegate?.togglePrefix("- ")
    }

    @FXML
    private fun onCodeButtonClick() {
        delegate?.toggleSelectionSurround("`")
    }

    @FXML
    private fun onHyperlinkButtonClick() {
        delegate?.toggleSelectionSurround("[", "](https://google.ca)")
    }

    @FXML
    private fun onHeadingMenuH1selected(){
        delegate?.togglePrefix("# ")
    }

    @FXML
    private fun onHeadingMenuH2selected(){
        delegate?.togglePrefix("## ")
    }

    @FXML
    private fun onHeadingMenuH3selected(){
        delegate?.togglePrefix("### ")
    }

    @FXML
    private fun onHeadingMenuH4selected(){
        delegate?.togglePrefix("#### ")
    }

    @FXML
    private fun onHeadingMenuH5selected(){
        delegate?.togglePrefix("##### ")
    }

    @FXML
    private fun onHeadingMenuH6selected(){
        delegate?.togglePrefix("###### ")
    }

    @FXML
    private fun onLivePreviewButtonClick() {
        toggleControllerDelegate?.togglePreview()
    }
}
