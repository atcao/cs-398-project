package com.caohusaad.family.tiger.models

import com.caohusaad.family.tiger.APIClient
import java.io.File
import kotlinx.coroutines.*
import net.lingala.zip4j.*

class FileModel(private val path: String = "notes/", private val exportPath: String = "exports/"): FileNodeDelegate {
    private var rootFileNode: FileNode = FileNode(File(path), mutableListOf<FileNode>(), null)

    init {
        refresh()
    }

    fun refresh() {
        rootFileNode = traverse(File(path), null)
    }

    // traverses the file path to regenerate the file tree
    private fun traverse(currFile: File, parent: FileNode?): FileNode {
        if (currFile.isFile) {
            val node = FileNode(currFile, mutableListOf(), parent)
            node.delegate = this
            return node
        }

        // Should be a directory now
        val res = FileNode(currFile, mutableListOf(), parent)
        val folderContents = res.file.listFiles() ?: throw Error("Trying to get contents of non-directory")
        res.delegate = this

        folderContents.forEach {
            if (it.isDirectory || it.name.endsWith(".md")) {
                res.children.add(traverse(it, res))
            }
        }

        return res
    }

    // returns the root file node for the notes directory
    fun getRootFileNode(): FileNode {
        return rootFileNode
    }

    fun getLastSavedTimestamp(exportZipDir: String = exportPath): Int? {
        val dir = File(exportZipDir)
        val exportDirContents = dir.listFiles()
        if (exportDirContents.isEmpty()) {
            return null
        }
        exportDirContents.forEach {
            if (it.isFile && it.name.endsWith(".zip")) {
                val timestamp = it.name.subSequence(0, it.name.indexOf("."))
                return timestamp.toString().toIntOrNull()
            }
        }
        return null
    }

    fun saveNotesZip(data: ByteArray, timestamp: Int, zipDir: String = exportPath) {
        deleteExistingNotesZip(zipDir)
        val notesZipPath = "${zipDir}/${timestamp}.zip"
        val notesZip = File(notesZipPath)
        notesZip.writeBytes(data)
    }

    fun replaceLocalFilesWithZipContent(localFilesDir: String = path, zipFileDir: String = exportPath) {
        deleteAllFilesInDir(localFilesDir)
        val dir = File(zipFileDir)
        val exportDirContents = dir.listFiles()
        exportDirContents.forEach {
            if (it.isFile && it.name.endsWith(".zip")) {
                val zip = ZipFile(it.path.toString())
                zip.extractAll(localFilesDir)
                return
            }
        }
    }

    override fun didSaveFile() {
        GlobalScope.launch {
            deleteExistingNotesZip()
            val timestamp = System.currentTimeMillis() / 1000L
            val zipFile = zipUpLocalNotes(path, "${exportPath}/${timestamp}.zip")
            APIClient.saveNotes("${timestamp}.zip", zipFile.readBytes())
        }
    }

    override fun didDeleteFile() {
        didSaveFile()
    }

    fun deleteExistingNotesZip(existingZipDir: String = exportPath) {
        val dir = File(existingZipDir)
        val exportDirContents = dir.listFiles()
        exportDirContents.forEach {
            if (it.isFile && it.name.endsWith(".zip")) {
                it.delete()
            }
        }
    }

    fun deleteAllFilesInDir(dirPath: String = path) {
        val dir = File(dirPath)
        if (dir.isFile) {
            dir.delete()
            return
        }
        val dirContents = dir.listFiles()
        dirContents.forEach {
            if (it.name != ".gitkeep")
            it.deleteRecursively()
        }
    }

    private fun zipUpLocalNotes(localNotesDir: String = path, zipFileOutputPath: String): File {
        val zip = ZipFile(zipFileOutputPath)
        val notesDir = File(localNotesDir)
        notesDir.listFiles().forEach {
            if (it.isFile) {
                zip.addFile(it)
            } else {
                zip.addFolder(it)
            }
        }
        return zip.file
    }
}