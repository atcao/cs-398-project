package com.caohusaad.family.tiger.models

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty


class NoteModel(noteContent: String) {

    private var noteContent: StringProperty = SimpleStringProperty(noteContent)

    fun noteContentProperty(): StringProperty {
        return noteContent
    }

    fun toggleSelectionSurround(prefix: String, suffix: String, start: Int, end: Int) {

        val text = noteContent.get()

        if (start == end) return

        val newText: String
        var newStart = start
        var newEnd = end
        if (isSurroundedBy(prefix, suffix, newStart, newEnd)) {
            newStart -= prefix.length
            newEnd += suffix.length
            newText = "${text.subSequence(start, end)}"
        } else {
            newText = "$prefix${text.subSequence(newStart, newEnd)}$suffix"
        }
        noteContent.set(text.replaceRange(newStart, newEnd, newText))
    }

    fun togglePrefix(prefix: String, start: Int, end: Int) {
        val text = noteContent.get()

        val snappedStart = text.subSequence(0, start).lastIndexOf('\n', end)+1

        if (text.substring(snappedStart).startsWith(prefix)){
            noteContent.set(text.replaceRange(snappedStart, snappedStart + prefix.length, ""))
        } else {
            noteContent.set(text.replaceRange(snappedStart, snappedStart, prefix))
        }
    }

    private fun isSurroundedBy(prefix: String, suffix: String, start: Int, end: Int): Boolean {
        // check left side
        var l = start-prefix.length
        var r = start
        if (l < 0) return false
        var subSequence = noteContent.get().subSequence(l, r)
        if (subSequence != prefix || (l > 0 && noteContent.get()[l-1] == '\\')) return false

        // check right side
        l = end
        r = end+suffix.length
        if (r > noteContent.get().length) return false
        subSequence = noteContent.get().subSequence(l, r)
        if (subSequence != suffix || (l > 0 && noteContent.get()[l-1] == '\\')) return false

        return true
    }
}
