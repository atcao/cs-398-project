package com.caohusaad.family.tiger.controllers

import javafx.fxml.FXML
import javafx.scene.control.RadioMenuItem

interface SidebarHeaderControllerDelegate {
    fun newFolderButtonClicked()
    fun newFileButtonClicked()
    fun sortLastModifiedClicked()
    fun sortCreationDateClicked()
    fun sortNameClicked()
}

class SidebarHeaderController {

    var delegate: SidebarHeaderControllerDelegate? = null

    @FXML
    lateinit var sortingMenuItem: RadioMenuItem

    // Event Handlers
    @FXML
    private fun onNewFolderButtonClick() {
        delegate?.newFolderButtonClicked()
    }

    @FXML
    private fun onNewFileButtonClick() {
        delegate?.newFileButtonClicked()
    }

    @FXML
    private fun sortLastModifiedClicked() {
        delegate?.sortLastModifiedClicked()
    }

    @FXML
    private fun sortCreationDateClicked() {
        delegate?.sortCreationDateClicked()
    }
    @FXML
    private fun sortNameClicked() {
        delegate?.sortNameClicked()
    }
}