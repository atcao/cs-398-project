package com.caohusaad.family.tiger

import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpHeaders
import java.net.http.HttpRequest
import java.net.http.HttpResponse

enum class HTTPMethod {
    GET, PUT
}

enum class APIEndpoint(val method: HTTPMethod, val path: String) {
    GET_NOTES_TIMESTAMP(HTTPMethod.GET, "/notes-timestamp"),
    GET_NOTES(HTTPMethod.GET, "/notes"),
    SAVE_NOTES(HTTPMethod.PUT, "/notes")
}

object APIClient {
    private var baseUrl = ""
    private val client = HttpClient.newBuilder().build()
    var isLoggedIn = false
    var userId = "undefined"


    fun setBaseURL(url: String) {
        baseUrl = url
    }

    fun getNotesLastSaveTimestamp(): Int? {
        if (!isLoggedIn) return null
        try {
            val headers = arrayOf<String>("userId", userId)
            val response = client.send(
                buildRequest(APIEndpoint.GET_NOTES_TIMESTAMP, headers = headers),
                HttpResponse.BodyHandlers.ofString()
            );
            return JSONObject(response.body()).get("timestamp") as? Int
        } catch (e: Exception) {
            print("Failed to complete request for getNotesLastSaveTimestamp: ")
            println(e)
        }
        return null
    }

    fun getNotes(): ByteArray? {
        if (!isLoggedIn) return null
        try {
            val headers = arrayOf<String>("userId", userId)
            val response = client.send(
                buildRequest(APIEndpoint.GET_NOTES, headers = headers),
                HttpResponse.BodyHandlers.ofByteArray()
            );
            return response.body()
        } catch (e: Exception) {
            print("Failed to complete request for getNotes: ")
            println(e)
        }
        return null
    }

    fun saveNotes(filename: String, data: ByteArray) {
        if (!isLoggedIn) return
        val headers = arrayOf<String>("Content-Type", "application/zip", "filename", "${userId}-${filename}")
        try {
            client.send(
                buildRequest(APIEndpoint.SAVE_NOTES, headers, data),
                HttpResponse.BodyHandlers.ofString()
            );
        } catch (e: Exception) {
            print("Failed to complete request for saveNotes: ")
            println(e)
        }
    }

    private fun buildRequest(
        endpoint: APIEndpoint,
        headers : Array<String> = arrayOf(""),
        body: ByteArray = byteArrayOf()
    ): HttpRequest {
        return when (endpoint) {
            APIEndpoint.GET_NOTES_TIMESTAMP -> {
                HttpRequest.newBuilder(URI.create(baseUrl + endpoint.path)).headers(*headers).build()
            }
            APIEndpoint.GET_NOTES -> {
                HttpRequest.newBuilder(URI.create(baseUrl + endpoint.path)).headers(*headers).build()
            }
            APIEndpoint.SAVE_NOTES -> {
                HttpRequest.newBuilder(URI.create(baseUrl + endpoint.path))
                    .PUT(HttpRequest.BodyPublishers.ofByteArray(body))
                    .headers(*headers)
                    .build()
            }
        }
    }



}