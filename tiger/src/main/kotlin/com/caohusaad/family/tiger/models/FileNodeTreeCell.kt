package com.caohusaad.family.tiger.models

import com.caohusaad.family.tiger.TigerApplication
import com.caohusaad.family.tiger.controllers.SidebarController
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.control.*
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.KeyCode

class FileNodeTreeCell(private val sidebarController: SidebarController) : TreeCell<FileNode?>() {
    private var textField: TextField
    private val fileIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/fileIcon.png"))
    private val folderIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/folderIcon.png"))

    init {
        textField = TextField(getCellLabel())
        textField.setOnKeyReleased {
            when(it.code) {
                KeyCode.ENTER -> {
                    item?.renameFile(textField.text)
                    commitEdit(item)
                }
                KeyCode.ESCAPE -> {
                    cancelEdit()
                }
                else -> {}
            }
        }
        textField.styleClass.add("sidebar-text-field")
    }

    private fun createFileEventHandler(isFile: Boolean): EventHandler<ActionEvent> {
        return EventHandler {
            var currTreeItem = treeItem
            if (treeItem == null) currTreeItem = treeView.root
            else if (!treeItem.value!!.file.isDirectory && treeItem.parent != null) currTreeItem = treeItem.parent

            val newFile = if (isFile) {
                currTreeItem.value?.createNewNote()
            } else {
                currTreeItem.value?.createNewFolder()
            }
            if (newFile != null) {
                currTreeItem.children.add(TreeItem(newFile, if (isFile) ImageView(fileIcon) else ImageView(folderIcon)))
                currTreeItem.children.sortWith { a, b ->
                    when {
                        (a == null && b == null) -> 0
                        (a == null) -> -1
                        else -> {
                            a.value!!.compareTo(b.value!!)
                        }
                    }
                }
            }
        }
    }

    private fun deleteFileEventHandler(): EventHandler<ActionEvent> {
        return EventHandler {
            if (treeItem != null) {
                treeItem.value?.deleteNote()
                treeItem.parent.children.remove(treeItem)

                if (treeItem.previousSibling() != null)
                    sidebarController.selected = treeItem.previousSibling().value!!
                else if (treeItem.nextSibling() != null)
                    sidebarController.selected = treeItem.nextSibling().value!!
                else
                    sidebarController.selected = sidebarController.rootNode
            }
        }
    }

    private var dropdown: ContextMenu = ContextMenu()
    init {
        val newFileMenuItem = MenuItem("New File")
        newFileMenuItem.onAction = createFileEventHandler(true)
        val newFolderMenuItem = MenuItem("New Folder")
        newFolderMenuItem.onAction = createFileEventHandler(false)
        val deleteFileMenuItem = MenuItem("Delete")
        deleteFileMenuItem.onAction = deleteFileEventHandler()
        dropdown.items.addAll(newFileMenuItem, newFolderMenuItem, deleteFileMenuItem)
        contextMenu = dropdown
    }

    private fun getCellLabel(): String {
        return if (item == null) "" else item.toString()
    }

    override fun startEdit() {
        super.startEdit()

        textField.text = getCellLabel()
        graphic = textField
        textField.selectAll()
        text = ""
    }

    override fun commitEdit(newValue: FileNode?) {
        super.commitEdit(newValue)
        text = item.toString()
        graphic = treeItem.graphic
    }

    override fun cancelEdit() {
        super.cancelEdit()
        text = item.toString()
        graphic = treeItem.graphic
    }

    override fun updateItem(item: FileNode?, empty: Boolean) {
        super.updateItem(item, empty)

        if (empty) {
            text = null
            graphic = null
        } else if (isEditing) {
            text = null
            graphic = textField
        } else {
            text = getCellLabel()
            graphic = treeItem.graphic
        }
    }

    override fun updateSelected(selected: Boolean) {
        super.updateSelected(selected)

        if (selected && item != null) {
            sidebarController.onSelectNode(item!!)
        }
    }
}