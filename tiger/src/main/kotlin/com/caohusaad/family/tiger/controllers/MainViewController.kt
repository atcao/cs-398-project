package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.APIClient
import com.caohusaad.family.tiger.TigerApplication

import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.parser.ParserEmulationProfile
import com.vladsch.flexmark.util.ast.Node
import com.vladsch.flexmark.util.data.MutableDataSet
import javafx.fxml.FXML
import javafx.scene.control.SplitPane
import javafx.scene.layout.BorderPane
import javafx.scene.web.WebView
import javafx.fxml.FXMLLoader
import javafx.stage.Popup

class MainViewController : NoteTextDelegate, ToggleControllerDelegate, AuthPopupDelegate, MainViewControllerDelegate {

    @FXML
    lateinit var menuBarController: MenuBarController

    @FXML
    private lateinit var noteEditorController: NoteEditorController

    @FXML
    private lateinit var sidebarController: SidebarController

    @FXML
    private lateinit var mainView: BorderPane

    @FXML
    lateinit var splitPane: SplitPane

    @FXML
    private lateinit var noteView: WebView

    private var popup: Popup? =null

    private var app: TigerApplication? = null

    private var splitPanePreviewPanelPosition: Double = 0.0

    fun initialize() {
        menuBarController.delegate = sidebarController
        menuBarController.mainViewDelegate = this
        sidebarController.delegate = noteEditorController

        noteEditorController.setNoteTextAreaControllerDelegate(this)
        noteEditorController.setToggleControllerDelegate(this)
        noteView.engine.userStyleSheetLocation =
            "file://${TigerApplication::class.java.getResource("stylesheets/Markdown.css").path}"
    }

    override fun textChanged(text: String) {
        val options = MutableDataSet()
        options.setFrom(ParserEmulationProfile.MARKDOWN)

        val parser: Parser = Parser.builder(options).build()
        val renderer: HtmlRenderer = HtmlRenderer.builder(options).build()

        val document: Node = parser.parse(text)
        val html: String = renderer.render(document)
        noteView.engine.loadContent("<div class='.markdown-body'>${html}</div>", "text/html")
    }

    fun setApp(app: TigerApplication) {
        this.app = app
    }

    override fun showAuthPopup() {
        val fxmlLoader = FXMLLoader(TigerApplication::class.java.getResource("AuthPopup.fxml"))
        popup = Popup()
        popup?.content?.add(fxmlLoader.load())
        val anchorX = mainView.layoutX + mainView.width/2 - 350
        val anchorY = mainView.layoutY + mainView.height/2 - 250
        popup?.show(mainView, anchorX, anchorY)
        fxmlLoader.getController<AuthPopupController>().delegate = this
        fxmlLoader.getController<AuthPopupController>().initialize()
    }

    private fun closeAuthPopup() {
        popup?.content?.clear()
        popup?.hide()
    }

    override fun loggedIn(id: String) {
        APIClient.userId = id
        APIClient.isLoggedIn = true
        app?.syncNotes(this)
        closeAuthPopup()
    }

    override fun continueLocally() {
        // don't have to sync notes if want to continue locally
//        app?.syncNotes(this)
        closeAuthPopup()
        loadLocalFiles()
    }

    fun setSidebarSize(sidebarPosition: Double) {
        splitPane.setDividerPosition(0, sidebarPosition)
    }

    fun getSidebarSize(): Double {
        return splitPane.dividerPositions[0]
    }

    fun saveCurrentNote() {
        sidebarController.saveCurrentNote()
    }

    override fun togglePreview() {
        if (splitPane.items.size == 2) {
            splitPane.items.add(noteView)
            splitPane.setDividerPosition(1, splitPanePreviewPanelPosition)
        } else {
            splitPanePreviewPanelPosition = splitPane.dividerPositions[1]
            splitPane.items.remove(noteView)
        }
    }

    fun loadLocalFiles() {
        sidebarController.traverse()
    }

    fun getLocalFileTimestamp(): Int? {
        return sidebarController.getLastSavedTimestamp()
    }

    fun saveNotesZip(data: ByteArray, timestamp: Int) {
        sidebarController.saveNotesZip(data, timestamp)
    }

    fun replaceLocalFilesWithZipContent() {
        sidebarController.replaceLocalFilesWithZipContent()
    }
}