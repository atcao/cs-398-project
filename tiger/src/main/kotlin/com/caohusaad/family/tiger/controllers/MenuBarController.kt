package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.APIClient
import com.caohusaad.family.tiger.models.FileNode
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.stage.Stage

interface MenuBarControllerDelegate {
    fun createNewNoteFromSelected()
    fun saveCurrentNote()
    fun exportCurrentNote(stage: Stage)
    fun clearNotes()
}

interface MainViewControllerDelegate {
    fun showAuthPopup()
}

class MenuBarController {
    @FXML
    private lateinit var menuBar: MenuBar

    var delegate: MenuBarControllerDelegate? = null

    var mainViewDelegate: MainViewControllerDelegate? = null

    fun initialize() {
        val os = System.getProperty("os.name", "UNKNOWN")
        if (os.startsWith("Mac")) menuBar.useSystemMenuBarProperty().set(true)
    }

    // File Menu Handlers
    @FXML
    private fun onFileQuitClick() {
        val stage = menuBar.scene.window
        if (stage is Stage) stage.close()
    }

    // Edit Menu Handlers

    // Window Menu Handlers
    @FXML
    private fun onWindowMaximizeClick() {
        val stage = menuBar.scene.window
        if (stage is Stage) stage.isMaximized = true
    }

    @FXML
    private fun onWindowMinimizeClick() {
        val stage = menuBar.scene.window
        if (stage is Stage) stage.isIconified = true
    }

    @FXML
    private fun onNewButtonClick() {
        delegate?.createNewNoteFromSelected()
    }

    fun onSaveButtonClick() {
        delegate?.saveCurrentNote()
    }

    @FXML
    private fun onExportButtonClick() {
        val stage = menuBar.scene.window as Stage
        delegate?.exportCurrentNote(stage)
    }

    @FXML fun onLogoutButtonClick() {
        if (APIClient.isLoggedIn) {
            APIClient.userId = ""
            APIClient.isLoggedIn = false
            delegate?.clearNotes()
        }
    }

    @FXML fun onLoginButtonClick() {
        if (!APIClient.isLoggedIn) {
            mainViewDelegate?.showAuthPopup()
        }
    }
}