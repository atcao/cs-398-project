package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.FileComparators
import com.caohusaad.family.tiger.TigerApplication
import com.caohusaad.family.tiger.models.FileModel
import com.caohusaad.family.tiger.models.FileNode
import com.caohusaad.family.tiger.models.FileNodeTreeCell
import javafx.fxml.FXML
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.stage.FileChooser
import javafx.stage.Stage

interface SidebarControllerDelegate {
    fun didSelectNote(name: String, content: String)
    fun getCurrentNoteContent(): String
    fun clearNoteContent()
}

class SidebarController: MenuBarControllerDelegate, SidebarHeaderControllerDelegate {
    @FXML
    private lateinit var sidebarContent: VBox
    private var treeView: TreeView<FileNode> = TreeView<FileNode>()

    @FXML
    private lateinit var sidebarHeaderController: SidebarHeaderController

    var delegate: SidebarControllerDelegate? = null

    private val fileIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/fileIcon.png"))
    private val folderIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/folderIcon.png"))

    var fileModel: FileModel = FileModel()
    private var currentSortMethod = SortMethod.NAME
    var rootNode = fileModel.getRootFileNode()
    var selected = rootNode

    enum class SortMethod {
        LAST_MODIFIED, CREATION_DATE, NAME
    }

    fun initialize() {
        sidebarHeaderController.delegate = this
        sidebarHeaderController.sortingMenuItem.isSelected = true
        VBox.setVgrow(treeView, Priority.ALWAYS)
    }

    fun onSelectNode(nextSelected: FileNode) {
        saveCurrentNote()
        selected = nextSelected
        delegate?.didSelectNote(selected.file.name, selected.loadFile())
    }

    fun traverse() {
        fileModel.refresh()
        rootNode = fileModel.getRootFileNode()
        treeView.root = rootNode.getTreeCell()
        treeView.isEditable = true
        treeView.isShowRoot = false
        sidebarContent.children.clear()
        sidebarContent.children.add(treeView)
        treeView.setCellFactory { FileNodeTreeCell(this) }
    }

    private fun createNewFileFromSelected(isFile: Boolean) {
        treeView.selectionModel.selectedItems.forEach {
            var treeItem = it
            if (!it.value.file.isDirectory && it.parent != null) treeItem = it.parent
            val newFile = if (isFile) treeItem.value.createNewNote() else treeItem.value.createNewFolder()
            if (newFile != null) {
                treeItem.value.children.add(newFile)
                treeItem.children.add(TreeItem(newFile, if (isFile) ImageView(fileIcon) else ImageView(folderIcon)))
                sortTree(treeItem)
            }
            // we should only perform this on the first, in fact there should only be one selected at a time
            return
        }
        // no selected items means that the user is creating a note on startup
        val newFile = if (isFile) rootNode.createNewNote() else rootNode.createNewFolder()
        if (newFile != null) {
            rootNode.children.add(newFile)
            treeView.root.children.add(TreeItem(newFile, if (isFile) ImageView(fileIcon) else ImageView(folderIcon)))
            sortTree(treeView.root)
        }
    }

    // Loops through all selected notes and if they're a directory, it will create a new note under it
    override fun createNewNoteFromSelected() {
        createNewFileFromSelected(true)
    }

    // Loops through all selected notes and if they're a directory, it will create a new folder under it
    private fun createNewFolderFromSelected() {
        createNewFileFromSelected(false)
    }

    override fun clearNotes() {
        fileModel.deleteExistingNotesZip()
        fileModel.deleteAllFilesInDir()
        delegate?.clearNoteContent()
        traverse()
    }

    override fun saveCurrentNote() {
        val newContent = delegate?.getCurrentNoteContent() ?: ""
        selected.saveFile(newContent)
    }

    override fun exportCurrentNote(stage: Stage) {
        val fileChooser = FileChooser()
        fileChooser.title = "Save"
        val extFilter = FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf")
        fileChooser.extensionFilters.add(extFilter)
        val file = fileChooser.showSaveDialog(stage)?.absolutePath

        if (file != null) {
            delegate?.getCurrentNoteContent()?.let { selected.export(file, it) }
        }
    }

    private fun sortTree(node: TreeItem<FileNode>) {
        var comparator = FileComparators.nameComparator
        when (currentSortMethod) {
            SortMethod.NAME -> comparator = FileComparators.nameComparator
            SortMethod.CREATION_DATE -> comparator = FileComparators.creationDateComparator
            SortMethod.LAST_MODIFIED -> comparator = FileComparators.lastModifiedComparator
        }
        node.children.sortWith(comparator)
        node.children.forEach {
            sortTree(it)
        }
    }

    override fun newFileButtonClicked() {
        createNewNoteFromSelected()
    }

    override fun newFolderButtonClicked() {
        createNewFolderFromSelected()
    }

    fun getLastSavedTimestamp(): Int? {
        return fileModel.getLastSavedTimestamp()
    }

    fun saveNotesZip(data: ByteArray, timestamp: Int) {
        fileModel.saveNotesZip(data, timestamp)
    }

    fun replaceLocalFilesWithZipContent() {
        fileModel.replaceLocalFilesWithZipContent()
    }

    override fun sortLastModifiedClicked() {
        currentSortMethod = SortMethod.LAST_MODIFIED
        sortTree(treeView.root)
    }

    override fun sortCreationDateClicked() {
        currentSortMethod = SortMethod.CREATION_DATE
        sortTree(treeView.root)
    }

    override fun sortNameClicked() {
        currentSortMethod = SortMethod.NAME
        sortTree(treeView.root)
    }

}
