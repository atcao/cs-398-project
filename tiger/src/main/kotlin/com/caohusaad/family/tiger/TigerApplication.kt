package com.caohusaad.family.tiger

import com.caohusaad.family.tiger.controllers.MainViewController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage

class TigerApplication : Application() {

    override fun start(stage: Stage) {
        val settingHandler = SettingHandler()
        val settings = settingHandler.loadSettings("user-settings.json")
        APIClient.setBaseURL("http://localhost:8080")
        val fxmlLoader = FXMLLoader(TigerApplication::class.java.getResource("MainView.fxml"))
        val scene = Scene(fxmlLoader.load(), settings.getDouble("width"), settings.getDouble("height"))
        val controller = fxmlLoader.getController<MainViewController>()
        controller.setSidebarSize(settings.getDouble("sidebarPosition"))

        stage.title = "Tiger"
        stage.scene = scene
        settingHandler.setSettings(stage, settings, controller)
        stage.setOnCloseRequest {
            controller.saveCurrentNote()
            settingHandler.saveSettings(stage, controller)
        }

        stage.icons.add(Image(("file:icon.png")))

        stage.show()
        // Disable interaction while files are loading
        // Note: This is basically instant on a local server
        controller.setApp(this)
        if (!APIClient.isLoggedIn) {
            controller.showAuthPopup()
        } else {
            syncNotes(controller)
        }
    }

    fun syncNotes(controller: MainViewController) {
        // Get remote zip timestamp
        val remoteFileTimestamp = APIClient.getNotesLastSaveTimestamp()
        if (remoteFileTimestamp == null) {
            // If remote timestamp failed to be retrieved, proceed with local file store
            controller.loadLocalFiles()
            return
        }

        // Get local zip timestamp
        val localTimestamp = controller.getLocalFileTimestamp()
        if (localTimestamp == null) {
            // If local zip could not be retrieved, get remote notes
            val remoteNoteSave = APIClient.getNotes()
            if (remoteNoteSave == null) {
                // If remote notes failed to be retrieved, proceed with local file store
                controller.loadLocalFiles()
                return
            }

            controller.saveNotesZip(remoteNoteSave, remoteFileTimestamp)
            controller.replaceLocalFilesWithZipContent()
            controller.loadLocalFiles()
            return
        }

        // Compare timestamps and use newer zip file
        if ( localTimestamp >= remoteFileTimestamp as Int) {
            controller.loadLocalFiles()
            return
        }

        val remoteNoteSave = APIClient.getNotes()
        if (remoteNoteSave == null) {
            // If remote notes failed to be retrieved, proceed with local file store
            controller.loadLocalFiles()
            return
        }
        controller.saveNotesZip(remoteNoteSave, remoteFileTimestamp)
        controller.replaceLocalFilesWithZipContent()
        controller.loadLocalFiles()
    }
}

fun main() {
    Application.launch(TigerApplication::class.java)
}