package com.caohusaad.family.tiger

import com.caohusaad.family.tiger.controllers.MainViewController
import javafx.stage.Stage
import org.json.JSONObject
import java.io.File
import java.io.ObjectOutputStream
import java.math.BigDecimal

class SettingHandler {

    fun loadSettings(path: String): JSONObject {
        val fileName = File(path)
        var settings = JSONObject()

        if (fileName.exists()) {
            // if settings exist load them
            val jsonString = fileName.readText(Charsets.UTF_8)
            settings = JSONObject(jsonString)

        } else {
            // else go with default values
            settings.put("sidebarPosition", 0.25)
            settings.put("width", 1300.0)
            settings.put("height", 700.0)
            settings.put("maximized", false)
            settings.put("userId", "undefined")
            settings.put("isLoggedIn", false)
        }
        return settings
    }

    fun setSettings(stage: Stage, settings: JSONObject, controller: MainViewController) {
        if (settings.has("x") && settings.has("y")) {
            stage.x = settings.getDouble("x")
            stage.y = settings.getDouble("y")
        }

        stage.width = settings.getDouble("width")
        stage.height = settings.getDouble("height")
        stage.isMaximized = settings.getBoolean("maximized")

        if (settings.has("previewCollapsed")) {
            if (settings.getBoolean("previewCollapsed")) controller.togglePreview()

            settings.getJSONArray("dividerPositions").forEachIndexed{ index, it ->
                controller.splitPane.setDividerPosition(index, (it as BigDecimal).toDouble())
            }
        }
        APIClient.userId = settings.getString("userId")
        APIClient.isLoggedIn = settings.getBoolean("isLoggedIn")
    }

    fun saveSettings(stage: Stage, controller: MainViewController) {
        val fileName = File("user-settings.json")
        val settings = JSONObject()

        // put key value pairs into JSON object
        settings.put("sidebarPosition", controller.getSidebarSize())
        settings.put("x", stage.x.toString())
        settings.put("y", stage.y.toString())
        settings.put("width", stage.width.toString())
        settings.put("height", stage.height.toString())
        settings.put("maximized", stage.isMaximized.toString())
        settings.put("userId", APIClient.userId)
        settings.put("isLoggedIn", APIClient.isLoggedIn)

        settings.put("dividerPositions", controller.splitPane.dividerPositions)
        settings.put("previewCollapsed", controller.splitPane.items.size == 2)

        // save JSON object to file
        fileName.outputStream().write(settings.toString(4).toByteArray())
    }
}
