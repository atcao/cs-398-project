package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.models.NoteModel
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.control.TextArea

interface NoteTextDelegate {
    fun textChanged(text: String)
}

class NoteViewController: NoteToolbarControllerDelegate {

    private var noteModel: NoteModel? = null
    private var wordCount = 0
    private var charCount = 0

    @FXML
    private lateinit var wordCountLabel: Label

    @FXML
    private lateinit var charCountLabel: Label

    @FXML
    private lateinit var noteTextArea: TextArea
    var noteTextDelegate: NoteTextDelegate? = null

    fun initialize() {
        noteTextArea.textProperty().addListener{
                _, _, newValue ->
            noteTextDelegate?.textChanged(newValue)
        }
        noteTextArea.textProperty().addListener{
                _, _, newValue ->
            updateWordCount(newValue)
            updateCharCount(newValue)
            wordCountLabel.text = "Word Count: $wordCount"
            charCountLabel.text = "Character Count: $charCount"
        }
        updateWordCount(noteTextArea.text)
        updateCharCount(noteTextArea.text)
        wordCountLabel.text = "Word Count: $wordCount"
        charCountLabel.text = "Character Count: $charCount"
    }

    private fun updateWordCount(content: String) {
        if (content.isBlank()) {
            wordCount = 0
            return
        }

        // strip non-alphanumerical characters
        val tempContent = Regex("[^A-Za-z0-9 ]").replace(content, "")
        val wordArray: List<String> = tempContent.trim().split("\\s+".toRegex())

        wordCount = wordArray.size
    }

    private fun updateCharCount(content: String) {
        if (content.isBlank()) {
            charCount = 0
            return
        }

        // strip non-alphanumerical characters
        var tempContent = Regex("[^A-Za-z0-9 ]").replace(content, "")
        // strip spaces
        tempContent = tempContent.replace("\\s".toRegex(), "")
        charCount = tempContent.length
    }

    fun setNote(note: NoteModel) {
        noteTextArea.textProperty().bindBidirectional(note.noteContentProperty())
        noteModel = note
    }

    fun getText(): String {
        return noteModel?.noteContentProperty()?.get() ?: ""
    }

    fun getWordCount(): Int {
        return wordCount
    }

    override fun toggleSelectionSurround(text: String) {
        val selection = noteTextArea.selection
        noteModel?.toggleSelectionSurround(text, text, selection.start, selection.end)
    }

    override fun toggleSelectionSurround(prefix: String, suffix: String) {
        val selection = noteTextArea.selection
        noteModel?.toggleSelectionSurround(prefix, suffix, selection.start, selection.end)
    }

    override fun togglePrefix(prefix: String) {
        val selection = noteTextArea.selection
        noteModel?.togglePrefix(prefix, selection.start, selection.end)
    }

    fun clearNote() {
        noteModel?.noteContentProperty()?.set("")
    }
}
