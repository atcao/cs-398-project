package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.models.NoteModel
import javafx.fxml.FXML

class NoteEditorController: SidebarControllerDelegate {
    // controllers
    @FXML
    private lateinit var noteToolbarController: NoteToolbarController

    @FXML
    private lateinit var noteTextAreaController: NoteViewController

    fun initialize() {
        noteToolbarController.delegate = noteTextAreaController
    }

    fun setNoteTextAreaControllerDelegate(noteTextDelegate: NoteTextDelegate) {
        noteTextAreaController.noteTextDelegate = noteTextDelegate
    }

    fun setToggleControllerDelegate(toggleControllerDelegate: ToggleControllerDelegate) {
        noteToolbarController.toggleControllerDelegate = toggleControllerDelegate
    }

    override fun didSelectNote(name: String, content: String) {
        noteTextAreaController.setNote(NoteModel(content))
    }

    override fun getCurrentNoteContent(): String {
        return noteTextAreaController.getText()
    }

    override fun clearNoteContent() {
        noteTextAreaController.clearNote()
    }
}