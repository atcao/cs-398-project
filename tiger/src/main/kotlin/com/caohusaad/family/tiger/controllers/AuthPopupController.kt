package com.caohusaad.family.tiger.controllers

import com.caohusaad.family.tiger.APIClient
import javafx.beans.value.ChangeListener
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.web.WebView

interface AuthPopupDelegate {
    fun loggedIn(id: String)
    fun continueLocally()
}

class AuthPopupController {
    @FXML
    private lateinit var webView: WebView

    var delegate: AuthPopupDelegate? = null

    fun initialize() {
        webView.engine.load("http://localhost:8080/login")

        webView.engine.locationProperty().addListener { observable, oldValue, newValue ->
            if (!APIClient.isLoggedIn) {
                val index = newValue.indexOf("tigeruserid=")
                if (index >= 0) {
                    val id = newValue.substring(index+12)
                    delegate?.loggedIn(id)
                }
            }
        }
    }

    @FXML
    fun onContinueLocallyClick() {
        delegate?.continueLocally()
    }
}