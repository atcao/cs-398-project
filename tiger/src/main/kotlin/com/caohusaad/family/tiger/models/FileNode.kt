package com.caohusaad.family.tiger.models

import com.caohusaad.family.tiger.FileComparators
import com.caohusaad.family.tiger.TigerApplication
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder
import javafx.scene.control.TreeItem
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.attribute.FileTime

interface FileNodeDelegate {
    fun didSaveFile()
    fun didDeleteFile()
}

// A wrapper for the file class so we can keep a list of files in memory and create tree cells
class FileNode(var file: File, val children: MutableList<FileNode>, val parent: FileNode?): Comparable<FileNode> {
    private val fileIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/fileIcon.png"))
    private val folderIcon: Image = Image(TigerApplication::class.java.getResourceAsStream("icons/folderIcon.png"))

    var delegate: FileNodeDelegate? = null
    var displayName: String
    var creationDate: String

    init {
        if (parent == null) {
            // we don't want the root dir renamed....
            displayName = file.name
            creationDate = "0"

        } else {
            if (!hasCreationDateTimestamp()) {
                addCreationDateToFilename()
            }
            val filenameSplits = file.name.split("_", ignoreCase = false, limit = 2)
            creationDate = filenameSplits[0]
            displayName = filenameSplits[1]
        }
    }

    fun saveFile(content: String) {
        // check if file is a directory AND that note content is different from disk
        if (file.isFile && (content != file.readText(Charsets.UTF_8))) {
            file.outputStream().write(content.toByteArray())
            delegate?.didSaveFile()
        }
    }

    fun loadFile(): String {
        if (file.isFile) {
            return file.readText(Charsets.UTF_8)
        }
        return ""
    }

    fun renameFile(newName: String) {
        try {
            val newFileName = "${creationDate}_${newName}"
            file = File(Files.move(file.toPath(), file.toPath().resolveSibling(newFileName)).toString())
            displayName = newName
        } catch (e: IOException) {
            println("File with same name already exists.") // TODO: Create alert to let user know that it already exists
        }
    }

    fun getTreeCell(): TreeItem<FileNode> {
        var nodeGraphic = ImageView(fileIcon)
        if (file.isDirectory) nodeGraphic = ImageView(folderIcon)

        val res = TreeItem(this, nodeGraphic)
        if (file.isDirectory) {
            children.forEach { res.children.add(it.getTreeCell()) }
        }

        // sort alphabetically on startup
        res.children.sortWith(FileComparators.nameComparator)
        return res
    }

    fun export(filePath: String, noteContent: String) {
        // convert markdown string to html string
        val flavour = CommonMarkFlavourDescriptor()
        val parsedTree = MarkdownParser(flavour).buildMarkdownTreeFromString(noteContent)
        val html = HtmlGenerator(noteContent, parsedTree, flavour).generateHtml()

        // convert html string to pdf file
        FileOutputStream(filePath).use { os ->
            val builder = PdfRendererBuilder()
            builder.useFastMode()
            builder.withHtmlContent(html, filePath)
            builder.toStream(os)
            builder.run()
        }
    }

    private fun createNewFile(baseName: String, addSuffix: Boolean): File {
        val noteFolder = File(file.path)
        var noteFolderPath = noteFolder.absolutePath

        // Create note folder if it does not exist
        if (!noteFolder.exists()) {
            val newFolder = createNewFolder()
            noteFolderPath = newFolder?.file?.absolutePath
        }

        val filenames = File(noteFolderPath).listFiles().filter { it.name != ".gitkeep" }
            .map { it.name.split("_", ignoreCase = false, limit = 2)[1] }
        val suffix = if (addSuffix) ".md" else ""
        var fileNumber = 0
        var fileName = if (fileNumber == 0) "$baseName$suffix" else "$baseName$fileNumber$suffix"

        while(fileName in filenames) {
            fileNumber++
            fileName = "$baseName$fileNumber$suffix"
        }
        val now = System.currentTimeMillis() / 1000L
        return File("$noteFolderPath/${now}_$fileName")
    }

    fun createNewNote(): FileNode? {
        if (!file.isDirectory) return null
        val note = createNewFile("New File", true)
        note.createNewFile()
        val res = FileNode(note, mutableListOf(), this)
        res.delegate = delegate
        children.add(res)
        return res
    }

    fun deleteNote() {
        file.deleteRecursively()
        parent?.children?.remove(this)
        delegate?.didDeleteFile()
    }

    fun createNewFolder(): FileNode? {
        if (!file.isDirectory) return null
        val note = createNewFile("New Folder", false)
        note.mkdir()
        val node = FileNode(note, mutableListOf(), this)
        node.delegate = delegate
        return node
    }

    // compares by names, but gives precedence to folders
    override fun compareTo(other: FileNode): Int {
        if (file.isDirectory && other.file.isDirectory || (!file.isDirectory && !other.file.isDirectory)) {
            return displayName.compareTo(other.displayName)
        } else if (file.isDirectory) {
            return -1
        }
        return 1
    }

    override fun toString(): String {
        return displayName
    }

    private fun hasCreationDateTimestamp(): Boolean {
        return try {
            val filenameSplits = file.name.split("_")
            filenameSplits[0].toLong()
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun addCreationDateToFilename() {
        val creationTime = Files.getAttribute(file.toPath(), "creationTime") as FileTime
        creationDate = creationTime.toMillis().toString()
        renameFile(file.name)
    }
}