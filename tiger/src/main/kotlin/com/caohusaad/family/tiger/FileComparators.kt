package com.caohusaad.family.tiger

import com.caohusaad.family.tiger.models.FileNode
import javafx.scene.control.TreeItem

class FileComparators {
    companion object {

        val lastModifiedComparator = Comparator<TreeItem<FileNode>> { f1, f2 ->
            if (f1.value.file.isDirectory && f2.value.file.isDirectory) {
                f1.value.file.name.compareTo(f2.value.file.name)
            } else if (f1.value.file.isDirectory) {
                -1
            } else if (f2.value.file.isDirectory) {
                1
            } else {
                (f2.value.file.lastModified().compareTo(f1.value.file.lastModified()))
            }
        }

        val creationDateComparator = Comparator<TreeItem<FileNode>> { f1, f2 ->
            if (f1.value.file.isDirectory && f2.value.file.isDirectory) {
                f1.value.displayName.compareTo(f2.value.displayName)
            } else if (f1.value.file.isDirectory) {
                -1
            } else if (f2.value.file.isDirectory) {
                1
            } else {
                f2.value.creationDate.toLong().compareTo(f1.value.creationDate.toLong())
            }
        }

        val nameComparator = Comparator<TreeItem<FileNode>> { f1, f2 ->
            if (f1.value.file.isDirectory && f2.value.file.isDirectory) {
                f1.value.displayName.compareTo(f2.value.displayName)
            } else if (f1.value.file.isDirectory) {
                -1
            } else if (f2.value.file.isDirectory) {
                1
            } else {
                f1.value.displayName.compareTo(f2.value.displayName, true)
            }
        }
    }
}