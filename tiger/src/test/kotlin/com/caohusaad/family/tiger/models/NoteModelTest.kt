package com.caohusaad.family.tiger.models

import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.api.Assertions.*

internal class NoteModelTest {

    @ParameterizedTest(name = "Given \"{0}\" and \"{1}\", and toggling surround with characters and selection, it should mutate the string to {2}")
    @MethodSource("toggleSelectionSurroundArguments")
    fun `given text selection and surrounding characters, toggle surrounding characters and mutate note content`(
        text: String, surroundChars: String, expected: String, selectionStart: Int, selectionEnd: Int
    ) {
        val noteModel = NoteModel(text)
        noteModel.toggleSelectionSurround(surroundChars, surroundChars, selectionStart, selectionEnd)
        assertEquals(noteModel.noteContentProperty().get(), expected)
    }

    private companion object {
        @JvmStatic
        fun toggleSelectionSurroundArguments() = listOf(
            // bold and unbold
            Arguments.of("test", "**", "**test**", 0, 4),
            Arguments.of("**test**", "**", "test", 2, 6),

            // 1 char surround
            Arguments.of("test", "_", "t_es_t", 1, 3),

            // surround checks till first whitespace
            Arguments.of("** test**", "**", "** **test****", 3, 7),

            // surround makes escaped character isn't replaced
            Arguments.of("\\_test_", "_", "\\__test__", 2, 6),

            // multiple word surround with multiple chars
            Arguments.of("this **_is_** a test", "`", "this **`_is_`** a test", 7, 11),
        )
    }
}