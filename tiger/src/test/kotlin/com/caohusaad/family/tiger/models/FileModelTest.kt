package com.caohusaad.family.tiger.models

import net.lingala.zip4j.ZipFile
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files

internal class FileModelTest {
    private val testFolderName: String = "testNoteFolder"
    private lateinit var testFolderPath: String
    private lateinit var model: FileModel

    @BeforeEach
    fun setUp() {
        val noteFolder = Files.createTempDirectory(testFolderName)
        testFolderPath = noteFolder.toString()
        model = FileModel(noteFolder.toString())
    }

    @Test
    fun `file is created in note folder` () {
        // Act
        val filenode = model.getRootFileNode().createNewNote()

        // Assert
        val testFilePath = File("$testFolderPath/${filenode?.file?.name}")
        assert(testFilePath.exists())
    }

    @Test
    fun `given note content, text file with name should be populated` () {
        // Act
        val newFile = model.getRootFileNode().createNewNote()
        newFile?.saveFile("test string")

        // Assert
        var testFile = File("${testFolderPath}/${newFile?.file?.name}")
        assert(testFile.exists())
        assertEquals("test string", testFile.inputStream().readBytes().toString(Charsets.UTF_8))
    }

    @Test
    fun `given note content, pdf with name should be generated` () {
        // Arrange
        val testFilePath = File("$testFolderPath/New File.pdf")

        // Act
        val newFile = model.getRootFileNode().createNewNote()
        newFile?.saveFile("test string")
        newFile?.export(testFilePath.absolutePath, "test string")

        // Assert
        assert(testFilePath.exists())
    }

    @Test
    fun `given directory with notes, file model should contain all notes` () {
        // Arrange
        for (i in 1 .. 3) {
            model.getRootFileNode().createNewNote()
        }

        // Act
        val fileNodes = model.getRootFileNode()
        val fileNodesNames = mutableListOf<String>()

        for (file in fileNodes.children) {
            fileNodesNames.add(file.displayName)
        }

        // Assert
        assert(fileNodesNames.size == 3)
        for (i in 0 .. 2) {
            assert(fileNodesNames.contains(if (i == 0) "New File.md" else "New File$i.md"))
        }
    }

    @Test
    fun `given new file, file is loaded into file model` () {
        // Arrange
        val testFileContent = "test string"
        val testFileName = "New File.md"
        val newFile = model.getRootFileNode().createNewNote()
        newFile!!.saveFile(testFileContent)

        // Act
        val contents = newFile.loadFile()

        // Assert
        assertEquals(newFile.displayName, testFileName)
        assertEquals(contents, testFileContent)
    }


    @Test
    fun `given directory with files and folders make sure everything is deleted and replaced with zip content` () {
        // Arrange
        val testDir = File(testFolderPath)
        File("${testDir.path}/should_be_deleted.txt").createNewFile()
        File("${testDir.path}/should_be_deleted1.txt").createNewFile()
        File("${testDir.path}/should_be_deleted_folder").mkdirs()
        File("${testDir.path}/should_be_deleted_folder/nested_file_test").createNewFile()
        assert(testDir.listFiles().size == 3)

        val zipTestDirPath = Files.createTempDirectory("zipDir")
        File("${zipTestDirPath.toString()}/test.txt").createNewFile()
        val zip = ZipFile("${zipTestDirPath.toString()}/test.zip")
        zip.addFile(File("${zipTestDirPath.toString()}/test.txt"))

        // Act
        model.replaceLocalFilesWithZipContent(testFolderPath.toString(), zipTestDirPath.toString())

        // Assert
        assertEquals(testDir.listFiles().size, 1)
        assert(testDir.listFiles().get(0).name == "test.txt")
    }

    @Test
    fun `given data, save data to zip file in specified directory` () {
        // Arrange
        val zipTestDirPath = Files.createTempDirectory("zipDir")
        val data: ByteArray = "test data".toByteArray()
        val timestamp = System.currentTimeMillis() / 1000L
        val expectedFilename = "${timestamp.toInt()}.zip"

        // Act
        model.saveNotesZip(data, timestamp.toInt(), zipTestDirPath.toString())

        // Assert
        val zipDir = File(zipTestDirPath.toString())
        assert(zipDir.listFiles().get(0).name == expectedFilename)
        assert(zipDir.listFiles().get(0).readBytes().contentEquals(data))
    }

    @Test
    fun `given directory without any zip files, no timestamp should be retrieved` () {
        // Arrange
        val testDir = File(testFolderPath)
        File("${testDir.path}/should_be_deleted.txt").createNewFile()
        File("${testDir.path}/should_be_deleted1.txt").createNewFile()
        File("${testDir.path}/should_be_deleted_folder").mkdirs()
        File("${testDir.path}/should_be_deleted_folder/nested_file_test").createNewFile()

        // Act
        val timestamp = model.getLastSavedTimestamp(testDir.path.toString())

        // Assert
        assert(timestamp == null)
    }

    @Test
    fun `given directory a zip file, a timestamp should be retrieved` () {
        // Arrange
        val testDir = File(testFolderPath)
        val timestamp = System.currentTimeMillis() / 1000L
        File("${testDir.path}/should_be_deleted.txt").createNewFile()
        File("${testDir.path}/${timestamp.toInt()}.zip").createNewFile()
        File("${testDir.path}/should_be_deleted_folder").mkdirs()
        File("${testDir.path}/should_be_deleted_folder/nested_file_test").createNewFile()

        // Act
        val retreivedTimestamp = model.getLastSavedTimestamp(testDir.path.toString())

        // Assert
        assert(retreivedTimestamp == timestamp.toInt())
    }
}

