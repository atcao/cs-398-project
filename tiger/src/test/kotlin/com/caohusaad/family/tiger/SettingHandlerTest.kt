package com.caohusaad.family.tiger

import org.json.JSONObject
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files

internal class SettingHandlerTest {

    private val testFolderName: String = "testNoteFolder"
    private lateinit var testFolderPath: String

    @Test
    fun `given user setting JSON file, JSONObject should be populated` () {
        // Arrange
        val settingHandler = SettingHandler()
        val noteFolder = Files.createTempDirectory(testFolderName)
        testFolderPath = noteFolder.toString()

        val testFilePath = File("$testFolderPath/test-settings.json")
        var settings = JSONObject()

        settings.put("sidebarPosition", 0.25)
        settings.put("x", 100.0)
        settings.put("y", 100.0)
        settings.put("width", 1000.0)
        settings.put("height", 500.0)
        settings.put("maximized", false)

        testFilePath.outputStream().write(settings.toString().toByteArray())

        // Act
        settings = settingHandler.loadSettings(testFilePath.toString())

        // Assert
        assert(settings.get("x") == 100)
        assert(settings.get("y") == 100)
        assert(settings.get("width") == 1000)
        assert(settings.get("height") == 500)
        assert(settings.get("maximized") == false)
    }
}